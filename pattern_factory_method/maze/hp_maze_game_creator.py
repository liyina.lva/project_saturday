# -*- coding: utf-8 -*-
from pattern_factory_method.door.harry_potter_door import HarryPotterDoor
from pattern_factory_method.maze.maze import Maze
from pattern_factory_method.maze.maze_game_creator import MazeGameCreator


class HpMazeGameCreator(MazeGameCreator):

    def create_maze(self):
        return self.make_maze()

    def make_maze(self):
        return Maze([HarryPotterDoor()])
