# -*- coding: utf-8 -*-
from abc import abstractmethod, ABC


class MazeGameCreator(ABC):

    @abstractmethod
    def create_maze(self):
        pass

    # def make_maze(self):
    #     pass
    #
    # @abstractmethod
    # def make_wall(self, wall):
    #     pass
    #
    # @abstractmethod
    # def make_room(self, room):
    #     pass
    #
    # @abstractmethod
    # def make_door(self, door):
    #     pass
