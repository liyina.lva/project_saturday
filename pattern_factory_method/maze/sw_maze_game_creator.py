# -*- coding: utf-8 -*-
from pattern_factory_method.maze.maze_game_creator import MazeGameCreator


class SwMazeGameCreator(MazeGameCreator):

    def make_wall(self):
        pass

    def make_room(self, room):
        room.draw()

    def make_door(self):
        pass
