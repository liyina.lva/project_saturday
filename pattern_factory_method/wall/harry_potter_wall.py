# -*- coding: utf-8 -*-
from pattern_factory_method.wall.wall import Wall


class HarryPotterWall(Wall):

    def draw(self):
        print('Harry potter wall')
