# -*- coding: utf-8 -*-
from pattern_factory_method.wall.wall import Wall


class SnowWhiteWall(Wall):

    def draw(self):
        print('Snow white wall')
