# -*- coding: utf-8 -*-
from abc import abstractmethod
from pattern_factory_method.map_site import MapSite


class Wall(MapSite):

    @abstractmethod
    def draw(self):
        pass
