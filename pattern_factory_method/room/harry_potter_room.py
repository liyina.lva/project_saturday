# -*- coding: utf-8 -*-
from pattern_factory_method.room.room import Room


class HarryPotterRoom(Room):

    def draw(self):
        print('Harry potter room')
