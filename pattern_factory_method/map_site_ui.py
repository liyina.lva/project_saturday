# -*- coding: utf-8 -*-
from pattern_factory_method.maze.hp_maze_game_creator import HpMazeGameCreator

if __name__ == "__main__":
    maze_creator = HpMazeGameCreator()
    maze = maze_creator.create_maze()
    maze.draw()
