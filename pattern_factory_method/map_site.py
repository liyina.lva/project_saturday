# -*- coding: utf-8 -*-
from abc import abstractmethod


class MapSite(object):

    @abstractmethod
    def draw(self):
        pass
