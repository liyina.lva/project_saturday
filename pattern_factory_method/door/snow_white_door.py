# -*- coding: utf-8 -*-
from pattern_factory_method.door.door import Door


class SnowWhiteDoor(Door):

    def draw(self):
        print('Snow white door')
