# -*- coding: utf-8 -*-
from pattern_factory_method.door.door import Door


class HarryPotterDoor(Door):

    def draw(self):
        print('Harry potter door')
