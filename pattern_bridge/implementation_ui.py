# -*- coding: utf-8 -
from pattern_bridge.bridge_mac_osx import BridgeMacOsx
from pattern_bridge.bridge_win_xp import BridgeWinXp
from pattern_bridge.mac_osx import MacOsx
from pattern_bridge.win_xp import WinXp


def main():
    osx = MacOsx()
    xp = WinXp()

    useCase = BridgeMacOsx(osx)
    useCase.someFunctionality()

    useCase = BridgeWinXp(xp)
    useCase.someFunctionality()

    # useCase = UseCase2(linux)
    # useCase.someFunctionality()
    #
    # useCase = UseCase2(windows)
    # useCase.someFunctionality()


if __name__ == "__main__":
    main()
