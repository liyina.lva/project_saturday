# -*- coding: utf-8 -*-
from pattern_bridge.bridge import Bridge


class BridgeWinXp(Bridge):

    def __init__(self, implementation):
        self.__implementation = implementation

    def bridgeWinXp(self):
        print("Bridge win xp: %", self.__implementation.anotherFunctionality())
