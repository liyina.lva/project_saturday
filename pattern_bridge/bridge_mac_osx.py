# -*- coding: utf-8 -*-
from pattern_bridge.bridge import Bridge


class BridgeMacOsx(Bridge):

    def __init__(self, implementation):
        self.__implementation = implementation

    def bridgeMacOsx(self):
        print("Bridge mac osx: %", self.__implementation.anotherFunctionality())
