# -*- coding: utf-8 -
from pattern_bridge.implementation_interface import ImplementationInterface


class MacOsx(ImplementationInterface):

    def anotherFunctionality(self):
        print ("Abre una ventana MacOsx!")
