# -*- coding: utf-8 -*-


class Implementation:

    def __init__(self):
        pass

    def someFunctionality(self):
        raise NotImplemented()
