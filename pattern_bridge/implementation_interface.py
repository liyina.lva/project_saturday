# -*- coding: utf-8 -

class ImplementationInterface:

    def anotherFunctionality(self):
        raise NotImplemented
