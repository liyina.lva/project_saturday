# -*- coding: utf-8 -*-
from pattern_visitor.node import Node


class Bin(Node):

    def __init__(self):
        super(Bin, self).__init__()
        self._node_left = []
        self._node_right = []
        self._value = 0

    def set_value(self, value):
        self._value = value

    def get_value(self):
        return self._value

    def printIt(self):
        print("Node bin")
