# -*- coding: utf-8 -*-
from pattern_composite.manager import Manager
from pattern_visitor.bin import Bin
from pattern_visitor.leaf import Leaf
from pattern_visitor.node import Node
from pattern_visitor.visitor import Visitor

if __name__ == "__main__":
    # n = Node()
    # n1 = Bin()
    # n2 = Leaf()
    # n3 = Bin()
    # n4 = Bin()
    #
    # n1.add(n3)
    # n1.add(n4)
    # n.add(n1)
    # n.add(n2)
    #
    visitor = Visitor()
    # visitor.visit(n)

    direct = Manager('Director Juan', '800')
    manager = Manager('  Manager Pedro', '700')
    developer1 = Manager('    Developer Maria', '500')
    developer2 = Manager('    Developer Ruben', '500')
    direct.add(manager)
    manager.add(developer1)
    manager.add(developer2)
    manager2 = Manager('  Manager Alicia', '700')
    developer3 = Manager('    Developer Pepe', '7800')
    direct.add(manager2)
    manager2.add(developer3)
    direct.print()

    direct.accept(visitor)
