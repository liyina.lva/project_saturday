# -*- coding: utf-8 -*-
from abc import ABCMeta


class Tree(object):
    __metaclass__ = ABCMeta

    def __init__(self, node):
        self._node = node

    def accept(self, visitor):
        pass
