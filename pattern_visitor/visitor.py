# -*- coding: utf-8 -*-
from abc import ABCMeta


class Visitor(object):
    __metaclass__ = ABCMeta

    def __init__(self):
        self._result = 0

    def visit(self, node):
        # node.printIt()
        if len(node._employees) > 0:
            for child in node._employees:
                child.accept(self)
