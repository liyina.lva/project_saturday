# -*- coding: utf-8 -*-
from pattern_visitor.node import Node


class Leaf(Node):

    def __init__(self):
        super(Leaf, self).__init__()
        self._value = 0

    def set_value(self, value):
        self._value = value

    def get_value(self):
        return self._value

    def printIt(self):
        print("Node leaf")
