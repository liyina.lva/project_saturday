# -*- coding: utf-8 -*-
from abc import ABCMeta


class Node(object):
    __metaclass__ = ABCMeta

    def __init__(self):
        self._node = []
        self._value = 0

    def add(self, node):
        self._node.append(node)

    def set_value(self, value):
        self._value = value

    def get_value(self):
        return self._value

    def accept(self, visitor):
        visitor.visit(self)

    def printIt(self):
        print("Node")
