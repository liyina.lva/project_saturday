# -*- coding: utf-8 -*-
from pattern_composite.employee import Employee


class Developer(Employee):

    def __init__(self, name, salary):
        super(Developer, self).__init__(name, salary)
