# -*- coding: utf-8 -*-
from pattern_composite.employee import Employee


class Manager(Employee):

    def __init__(self, name, salary):
        super(Manager, self).__init__(name, salary)
        self._employees = []

    def add(self, employee):
        self._employees.append(employee)

    def remove(self, employee):
        self._employees.remove(employee)

    def get_child(self):
        return self._employees.count()

    def print(self):
        super(Manager, self).print()
        for employee in self._employees:
            employee.print()
