# -*- coding: utf-8 -*-
from pattern_composite.developer import Developer
from pattern_composite.manager import Manager

if __name__ == "__main__":

    direct = Manager('Director Juan', '800')
    manager = Manager('  Manager Pedro', '700')
    developer1 = Manager('    Developer Maria', '500')
    developer2 = Manager('    Developer Ruben', '500')
    direct.add(manager)
    manager.add(developer1)
    manager.add(developer2)
    manager2 = Manager('  Manager Alicia', '700')
    developer3 = Manager('    Developer Pepe', '7800')
    direct.add(manager2)
    manager2.add(developer3)
    direct.print()
