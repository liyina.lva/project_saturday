# -*- coding: utf-8 -*-
from abc import ABCMeta, abstractmethod


class Employee(object):
    __metaclass__ = ABCMeta

    def __init__(self, name, salary):
        self._name = name
        self._salary = salary

    def get_name(self):
        return self._name

    def get_salary(self):
        return self._salary

    def print(self):
        print('Imprimir', (self._name, self._salary))

    def accept(self, visitor):
        visitor.visit(self)
