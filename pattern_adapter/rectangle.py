# -*- coding: utf-8 -*-
from abc import abstractmethod
from pattern_adapter.shape import Shape


class Rectangle(Shape):

    @abstractmethod
    def show(self):
        pass

    @abstractmethod
    def full(self):
        pass

    @abstractmethod
    def set_color(self, color):
        pass

    @abstractmethod
    def erase(self):
        pass

    @abstractmethod
    def set_place(self, place):
        pass

    @abstractmethod
    def get_place(self):
        pass
