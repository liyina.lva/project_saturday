# -*- coding: utf-8 -*-
from pattern_state.state_base import StateBase


class StateBaseA(StateBase):

    def open(self):
        print('En transición A')

    def in_state(self):
        print('En estado A')

    def close(self):
        print('A esta cerrado')
