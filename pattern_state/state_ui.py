# -*- coding: utf-8 -*
from pattern_state.state import State
from pattern_state.state_base_a import StateBaseA


def main():
    state_a = StateBaseA()
    state_a.open()
    state = State(state_a)
    state.enable()


if __name__ == "__main__":
    main()
