# -*- coding: utf-8 -*-
from pattern_state.state_base import StateBase


class StateBaseB(StateBase):

    def open(self):
        print('En transición B')

    def in_state(self):
        print('En estado B')

    def close(self):
        print('B esta cerrado')
