# -*- coding: utf-8 -*-
class State(object):

    def __init__(self, state):
        self._state = state

    def enable(self):
        self._state.open()
        self._state.in_state()
        self._state.close()
