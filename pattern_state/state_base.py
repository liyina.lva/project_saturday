# -*- coding: utf-8 -*-
import abc


class StateBase(object):

    @abc.abstractmethod
    def open(self):
        pass

    @abc.abstractmethod
    def in_state(self):
        pass

    @abc.abstractmethod
    def close(self):
        pass
