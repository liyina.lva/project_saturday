# -*- coding: utf-8 -*-
from abc import ABCMeta, abstractmethod


class Document(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def open(self):
        pass

    @abstractmethod
    def save(self):
        pass

    @abstractmethod
    def show(self, color):
        pass
