# -*- coding: utf-8 -*-
from pattern_proxy.document import Document


class DocumentReal(Document):

    def open(self):
        print('Abrir documento')

    def save(self):
        print('Guardar documento')

    def show(self):
        print('Mostrar documento')

