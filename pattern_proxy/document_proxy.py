# -*- coding: utf-8 -*-
from abc import abstractmethod
from pattern_proxy.document import Document


class DocumentProxy(Document):

    def __init__(self, document_real):
        self._document = document_real

    def open(self):
        self._document.open()

    def save(self):
        self._document.open()

    def show(self):
        self._document.open()
