# -*- coding: utf-8 -*-
from pattern_proxy.document_proxy import DocumentProxy
from pattern_proxy.document_real import DocumentReal

if __name__ == "__main__":

    document_real = DocumentReal()
    document = DocumentProxy(document_real)
    document.open()